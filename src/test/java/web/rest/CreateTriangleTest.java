package web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.CreateTriangleFullDto;
import dto.CreateTriangleSimpleDto;
import model.HttpException;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.ConnectionUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;

public class CreateTriangleTest {

    @BeforeEach
    public void setUp() {
        ConnectionUtil conn = new ConnectionUtil();

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();
        if (!triangles.isEmpty()) {
            for (Triangle triangle : triangles) {
                conn.deleteTriangleRequest(triangle.getId());
            }
        }
        try {
            conn.closeHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createTriangleWithoutSeparator() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse response;
        ObjectMapper mapper = new ObjectMapper();
        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(200,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle actualTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        assertNotNull(actualTriangle.getId());
        assertEquals(input.getFirstSide(), actualTriangle.getFirstSide());
        assertEquals(input.getSecondSide(), actualTriangle.getSecondSide());
        assertEquals(input.getThirdSide(), actualTriangle.getThirdSide());
    }

    @Test
    public void createTriangleWithSeparator() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator("!");
        input.setFirstSide(4.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse response;
        ObjectMapper mapper = new ObjectMapper();
        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(200,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle actualTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        assertNotNull(actualTriangle.getId());
        assertEquals(input.getFirstSide(), actualTriangle.getFirstSide());
        assertEquals(input.getSecondSide(), actualTriangle.getSecondSide());
        assertEquals(input.getThirdSide(), actualTriangle.getThirdSide());
    }

    @Test
    public void createTenTriangles() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(10.0);
        input.setSecondSide(2.0);
        input.setThirdSide(9.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        ObjectMapper mapper = new ObjectMapper();

        for (int i = 0; i < 10; i++) {
            CloseableHttpResponse response;
            response = conn.getHttpClient().execute(post);
            String entity = EntityUtils.toString(response.getEntity());

            assertEquals(200,
                    response.getStatusLine().getStatusCode(),
                    "HTTP status is not as expected and error is:" + entity);

            Triangle actualTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
            });

            assertNotNull(actualTriangle.getId());
            assertEquals(input.getFirstSide(), actualTriangle.getFirstSide());
            assertEquals(input.getSecondSide(), actualTriangle.getSecondSide());
            assertEquals(input.getThirdSide(), actualTriangle.getThirdSide());
        }

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();

        assertEquals(triangles.size(), 10);
    }

    @Test
    public void createElevenTriangles() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(10.0);
        input.setSecondSide(2.0);
        input.setThirdSide(9.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        ObjectMapper mapper = new ObjectMapper();

        for (int i = 0; i < 10; i++) {
            CloseableHttpResponse response;
            response = conn.getHttpClient().execute(post);
            String entity = EntityUtils.toString(response.getEntity());

            assertEquals(200,
                    response.getStatusLine().getStatusCode(),
                    "HTTP status is not as expected and error is:" + entity);

            Triangle actualTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
            });

            assertNotNull(actualTriangle.getId());
            assertEquals(input.getFirstSide(), actualTriangle.getFirstSide());
            assertEquals(input.getSecondSide(), actualTriangle.getSecondSide());
            assertEquals(input.getThirdSide(), actualTriangle.getThirdSide());
        }

        CloseableHttpResponse eleventhResponse;

        eleventhResponse = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(eleventhResponse.getEntity());

        assertEquals(422,
                eleventhResponse.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unprocessable Entity", actual.getError());
        assertEquals("com.natera.test.triangle.exception.LimitExceededException", actual.getException());
        assertEquals("Limit exceeded", actual.getMessage());

    }

    @Test
    public void createWrongTriangle() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(1.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        ObjectMapper mapper = new ObjectMapper();

        CloseableHttpResponse response;

        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(422,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unprocessable Entity", actual.getError());
        assertEquals("com.natera.test.triangle.exception.UnprocessableDataException", actual.getException());
        assertEquals("Cannot process input", actual.getMessage());

    }

    @Test
    public void createTriangleByNonAuthorisedUser() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(5.0);
        input.setSecondSide(3.0);
        input.setThirdSide(3.0);

        Header[] headers = new Header[2];
        headers[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        headers[1] = new BasicHeader("X-User", "non valid header");

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequestWithCustomHeaders(input.toString(), headers);
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpResponse response;

        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(401,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unauthorized", actual.getError());
        assertEquals("No message available", actual.getMessage());
    }

    @Test
    public void createTriangleWithNegativeSide() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(-5.0);
        input.setSecondSide(3.0);
        input.setThirdSide(3.0);


        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpResponse response;

        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(422,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unprocessable Entity", actual.getError());
        assertEquals("com.natera.test.triangle.exception.UnprocessableDataException", actual.getException());
        assertEquals("Cannot process input", actual.getMessage());
    }

    @Test
    public void createTriangleUsingNonValidRequest() throws IOException {
        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator("");
        input.setFirstSide(4);
        input.setSecondSide(3);
        input.setThirdSide(2);


        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toStringAsIntegerValues());
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpResponse response;

        response = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(422,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unprocessable Entity", actual.getError());
        assertEquals("com.natera.test.triangle.exception.UnprocessableDataException", actual.getException());
        assertEquals("Cannot process input", actual.getMessage());
    }
}
