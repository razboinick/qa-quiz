package web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.CreateTriangleSimpleDto;
import model.HttpException;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.ConnectionUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteTriangleTest {

    @BeforeEach
    public void setUp() {
        ConnectionUtil conn = new ConnectionUtil();

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();
        if (!triangles.isEmpty()) {
            for (Triangle triangle : triangles) {
                conn.deleteTriangleRequest(triangle.getId());
            }
        }
        try {
            conn.closeHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteExistingTriangle() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responseCreate;
        CloseableHttpResponse responseDelete;
        ObjectMapper mapper = new ObjectMapper();

        responseCreate = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responseCreate.getEntity());

        Triangle expectedTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });
        String id = expectedTriangle.getId();

        HttpDelete delete = conn.createDeleteRequest(id);
        responseDelete = conn.getHttpClient().execute(delete);
        String entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(200,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entityDelete);

        assertEquals(0, conn.getAllTrianglesRequest().size());
    }

    @Test
    public void deleteDeletedTriangle() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responseCreate;
        CloseableHttpResponse responseDelete;
        ObjectMapper mapper = new ObjectMapper();

        responseCreate = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responseCreate.getEntity());

        Triangle expectedTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });
        String id = expectedTriangle.getId();

        HttpDelete delete = conn.createDeleteRequest(id);
        responseDelete = conn.getHttpClient().execute(delete);
        String entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(200,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entityDelete);

        responseDelete = conn.getHttpClient().execute(delete);
        entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(200,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entityDelete);

        assertEquals(0, conn.getAllTrianglesRequest().size());
    }

    @Test
    public void deleteNonExistingTriangle() {
        UUID uuid = UUID.randomUUID();

        ConnectionUtil conn = new ConnectionUtil();
        CloseableHttpResponse responseDelete;
        ObjectMapper mapper = new ObjectMapper();

        try {
            HttpDelete delete = conn.createDeleteRequest(uuid.toString());
            responseDelete = conn.getHttpClient().execute(delete);
            String entity = EntityUtils.toString(responseDelete.getEntity());

            //todo: I think delete non existing entity should return 404, but it is a philosophical issue.
            assertEquals(200,
                    responseDelete.getStatusLine().getStatusCode(),
                    "HTTP status is not as expected and error is:" + entity);

            assertEquals(0, conn.getAllTrianglesRequest().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteTriangleBYNonAuthorisedUser() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responseCreate;
        CloseableHttpResponse responseDelete;
        ObjectMapper mapper = new ObjectMapper();

        Header[] headers = new Header[2];
        headers[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        headers[1] = new BasicHeader("X-User", "non valid header");

        responseCreate = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responseCreate.getEntity());
        Triangle expectedTriangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        String id = expectedTriangle.getId();

        HttpDelete delete = conn.createDeleteRequestWithCustomHeaders(id, headers);
        responseDelete = conn.getHttpClient().execute(delete);
        String entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(401,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entityDelete, new TypeReference<HttpException>() {
        });

        assertEquals("Unauthorized", actual.getError());
        assertEquals("No message available", actual.getMessage());

    }
}

