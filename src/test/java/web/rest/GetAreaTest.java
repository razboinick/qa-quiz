package web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.CreateTriangleFullDto;
import dto.CreateTriangleSimpleDto;
import model.HttpException;
import model.Result;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.ConnectionUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetAreaTest {

    @BeforeEach
    public void setUp() {
        ConnectionUtil conn = new ConnectionUtil();

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();
        if (!triangles.isEmpty()) {
            for (Triangle triangle : triangles) {
                conn.deleteTriangleRequest(triangle.getId());
            }
        }
        try {
            conn.closeHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAreaForIntegerSides() throws IOException {

        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toStringAsIntegerValues());

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetAreaRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(200,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + actualEntity);


        Result actual = mapper.readValue(actualEntity, new TypeReference<Result>() {
        });

        assertEquals(1.984313483298443, actual.getResult());

    }

    @Test
    public void getAreaForDoubleSides() throws IOException {

        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(3.22);
        input.setSecondSide(3.3);
        input.setThirdSide(5.578);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetAreaRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(200,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + actualEntity);


        Result actual = mapper.readValue(actualEntity, new TypeReference<Result>() {
        });

        assertEquals(4.707128881362714, actual.getResult());
    }

    @Test
    public void getAreaOfDeletedTriangle() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responseCreate;
        CloseableHttpResponse responseDelete;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();

        responseCreate = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responseCreate.getEntity());

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });
        String id = triangle.getId();

        HttpDelete delete = conn.createDeleteRequest(id);
        responseDelete = conn.getHttpClient().execute(delete);
        String entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(200,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entityDelete);

        HttpGet get = conn.createGetAreaRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(404,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);

        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Not Found", actual.getError());
        assertEquals("com.natera.test.triangle.exception.NotFounException", actual.getException());
        assertEquals("Not Found", actual.getMessage());
        assertEquals("/triangle/" + triangle.getId() + "/area", actual.getPath());
    }

    @Test
    public void getAreaOfNonExistingTriangle() throws IOException {
        UUID id = UUID.randomUUID();
        ConnectionUtil conn = new ConnectionUtil();

        HttpGet get = conn.createGetAreaRequest(id.toString());

        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();

        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(404,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);

        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Not Found", actual.getError());
        assertEquals("com.natera.test.triangle.exception.NotFounException", actual.getException());
        assertEquals("Not Found", actual.getMessage());
        assertEquals("/triangle/" + id.toString() + "/area", actual.getPath());
    }

    @Test
    public void getAreaByNonAuthorisedUser() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        Header[] headers = new Header[2];
        headers[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        headers[1] = new BasicHeader("X-User", "non valid header");

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetAreaWithCustomerHeadersRequest(triangle.getId(), headers);
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(401,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);


        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Unauthorized", actual.getError());
        assertEquals("No message available", actual.getMessage());
    }
}
