package web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.HttpException;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.ConnectionUtil;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetAllTrianglesTest {

    @BeforeEach
    public void setUp() {
        ConnectionUtil conn = new ConnectionUtil();

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();
        if (!triangles.isEmpty()) {
            for (Triangle triangle : triangles) {
                conn.deleteTriangleRequest(triangle.getId());
            }
        }
        try {
            conn.closeHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllTrianglesByNonAuthorisedUser() throws IOException {

        ConnectionUtil conn = new ConnectionUtil();

        CloseableHttpResponse response;
        ObjectMapper mapper = new ObjectMapper();

        Header[] headers = new Header[2];
        headers[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        headers[1] = new BasicHeader("X-User", "non valid header");

        HttpGet get = conn.createGetAllRequestWithCustomHeaders(headers);

        response = conn.getHttpClient().execute(get);
        String entity = EntityUtils.toString(response.getEntity());

        assertEquals(401,
                response.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and response is:" + entity);

        HttpException actual = mapper.readValue(entity, new TypeReference<HttpException>() {
        });

        assertEquals("Unauthorized", actual.getError());
        assertEquals("No message available", actual.getMessage());
    }
}
