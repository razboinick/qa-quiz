package web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.CreateTriangleFullDto;
import dto.CreateTriangleSimpleDto;
import model.HttpException;
import model.Result;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.ConnectionUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetPerimeterTest {

    @BeforeEach
    public void setUp() {
        ConnectionUtil conn = new ConnectionUtil();

        ArrayList<Triangle> triangles = conn.getAllTrianglesRequest();
        if (!triangles.isEmpty()) {
            for (Triangle triangle : triangles) {
                conn.deleteTriangleRequest(triangle.getId());
            }
        }
        try {
            conn.closeHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPerimeterForIntegerSides() throws IOException {

        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toStringAsIntegerValues());

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetPerimeterRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(200,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + actualEntity);


        Result actual = mapper.readValue(actualEntity, new TypeReference<Result>() {
        });

        assertEquals(7.0, actual.getResult());
    }

    @Test
    public void getPerimeterForDoubleSides() throws IOException {

        CreateTriangleFullDto input = new CreateTriangleFullDto();
        input.setSeparator(";");
        input.setFirstSide(2.4);
        input.setSecondSide(2.3);
        input.setThirdSide(3.578);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetPerimeterRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(200,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + actualEntity);


        Result actual = mapper.readValue(actualEntity, new TypeReference<Result>() {
        });

        assertEquals(8.278, actual.getResult());
    }

    @Test
    public void getPerimeterOfDeletedTriangle() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());

        CloseableHttpResponse responseCreate;
        CloseableHttpResponse responseDelete;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();

        responseCreate = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responseCreate.getEntity());

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });
        String id = triangle.getId();

        HttpDelete delete = conn.createDeleteRequest(id);
        responseDelete = conn.getHttpClient().execute(delete);
        String entityDelete = EntityUtils.toString(responseDelete.getEntity());

        assertEquals(200,
                responseDelete.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entityDelete);

        HttpGet get = conn.createGetPerimeterRequest(triangle.getId());
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(404,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);

        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Not Found", actual.getError());
        assertEquals("com.natera.test.triangle.exception.NotFounException", actual.getException());
        assertEquals("Not Found", actual.getMessage());
        assertEquals("/triangle/" + triangle.getId() + "/perimeter", actual.getPath());
    }

    @Test
    public void getPerimeterOfNonExistingTriangle() throws IOException {
        UUID id = UUID.randomUUID();
        ConnectionUtil conn = new ConnectionUtil();

        HttpGet get = conn.createGetPerimeterRequest(id.toString());

        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();

        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(404,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);

        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Not Found", actual.getError());
        assertEquals("com.natera.test.triangle.exception.NotFounException", actual.getException());
        assertEquals("Not Found", actual.getMessage());
        assertEquals("/triangle/" + id.toString() + "/perimeter", actual.getPath());
    }

    @Test
    public void getPerimeterByNonAuthorisedUser() throws IOException {
        CreateTriangleSimpleDto input = new CreateTriangleSimpleDto();
        input.setFirstSide(2.0);
        input.setSecondSide(2.0);
        input.setThirdSide(3.0);

        ConnectionUtil conn = new ConnectionUtil();

        HttpPost post = conn.createTriangleRequest(input.toString());
        Header[] headers = new Header[2];
        headers[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        headers[1] = new BasicHeader("X-User", "non valid header");

        CloseableHttpResponse responsePost;
        CloseableHttpResponse responseGet;

        ObjectMapper mapper = new ObjectMapper();
        responsePost = conn.getHttpClient().execute(post);
        String entity = EntityUtils.toString(responsePost.getEntity());

        assertEquals(200,
                responsePost.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and error is:" + entity);

        Triangle triangle = mapper.readValue(entity, new TypeReference<Triangle>() {
        });

        HttpGet get = conn.createGetPerimeterWithCustomerHeadersRequest(triangle.getId(), headers);
        responseGet = conn.getHttpClient().execute(get);
        String actualEntity = EntityUtils.toString(responseGet.getEntity());

        assertEquals(401,
                responseGet.getStatusLine().getStatusCode(),
                "HTTP status is not as expected and message is:" + actualEntity);


        HttpException actual = mapper.readValue(actualEntity, new TypeReference<HttpException>() {
        });

        assertEquals("Unauthorized", actual.getError());
        assertEquals("No message available", actual.getMessage());
    }
}
