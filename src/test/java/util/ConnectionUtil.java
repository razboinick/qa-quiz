package util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Triangle;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ConnectionUtil {

    private final CloseableHttpClient httpClient;

    //todo: make config instead of variable
    private final String NATERA_QUIZ_TRIANGLE_URI = "https://qa-quiz.natera.com/triangle";
    private final String GET_ALL_TRIANGLES = "all";
    private String GET_PERIMETER = "/perimeter";
    private String GET_AREA = "/area";
    private final String X_USER_HEADER_VALUE = "182462f5-3330-4632-b42a-21bd66c2b4a3";

    public ConnectionUtil() {
        this.httpClient = HttpClients.createDefault();
    }

    private Header[] initializeHeaders() {
        Header[] defaultHeaders = new Header[2];
        defaultHeaders[0] = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        defaultHeaders[1] = new BasicHeader("X-User", X_USER_HEADER_VALUE);

        return defaultHeaders;
    }

    public CloseableHttpClient getHttpClient() {
        return this.httpClient;
    }

    public void closeHttpClient() throws IOException {
        this.httpClient.close();
    }

    public HttpPost createTriangleRequest(String json) {
        HttpPost post = new HttpPost(NATERA_QUIZ_TRIANGLE_URI);
        try {
            post.setEntity(new StringEntity(json));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        post.setHeaders(initializeHeaders());
        return post;
    }

    public HttpPost createTriangleRequestWithCustomHeaders(String json, Header[] headers) {
        HttpPost post = new HttpPost(NATERA_QUIZ_TRIANGLE_URI);
        try {
            post.setEntity(new StringEntity(json));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        post.setHeaders(headers);
        return post;
    }

    public HttpGet createGetRequest(String id) {
        HttpGet get = new HttpGet(NATERA_QUIZ_TRIANGLE_URI+"/" + id);
        get.setHeaders(initializeHeaders());

        return get;
    }

    public HttpGet createGetPerimeterRequest(String id) {
        return createGetRequest(id + GET_PERIMETER);
    }

    public HttpGet createGetPerimeterWithCustomerHeadersRequest(String id, Header[] headers) {
        return createGetRequestWithCustomHeaders(id + GET_PERIMETER, headers);
    }

    public HttpGet createGetAreaRequest(String id) {
        return createGetRequest(id + GET_AREA);
    }

    public HttpGet createGetAreaWithCustomerHeadersRequest(String id, Header[] headers) {
        return createGetRequestWithCustomHeaders(id + GET_AREA, headers);
    }

    public HttpGet createGetRequestWithCustomHeaders(String id, Header[] headers) {
        HttpGet get = new HttpGet(NATERA_QUIZ_TRIANGLE_URI+"/" + id);
        get.setHeaders(headers);

        return get;
    }

    public HttpGet createGetAllRequestWithCustomHeaders(Header[] headers) {
        return createGetRequestWithCustomHeaders(GET_ALL_TRIANGLES, headers);
    }

    public HttpDelete createDeleteRequest(String id) {
        HttpDelete delete = new HttpDelete(NATERA_QUIZ_TRIANGLE_URI + "/" + id);
        delete.setHeaders(initializeHeaders());

        return delete;
    }

    public HttpDelete createDeleteRequestWithCustomHeaders(String id, Header[] headers) {
        HttpDelete delete = new HttpDelete(NATERA_QUIZ_TRIANGLE_URI + "/" + id);
        delete.setHeaders(headers);

        return delete;
    }

    public ArrayList<Triangle> getAllTrianglesRequest() {
        HttpGet get = createGetRequest(GET_ALL_TRIANGLES);
        CloseableHttpResponse response;
        ArrayList<Triangle> resultList = new ArrayList<Triangle>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            response = getHttpClient().execute(get);
            resultList = mapper.readValue(EntityUtils.toString(response.getEntity()),
                    new TypeReference<ArrayList<Triangle>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    public void deleteTriangleRequest(String id) {
        HttpDelete delete = createDeleteRequest(id);

        try {
            getHttpClient().execute(delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
