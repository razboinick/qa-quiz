package dto;

public class CreateTriangleSimpleDto {
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public double getFirstSide() {
        return firstSide;
    }

    public void setFirstSide(double firstSide) {
        this.firstSide = firstSide;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public void setSecondSide(double secondSide) {
        this.secondSide = secondSide;
    }

    public double getThirdSide() {
        return thirdSide;
    }

    public void setThirdSide(double thirdSide) {
        this.thirdSide = thirdSide;
    }

    @Override
    public String toString() {
        return "{" +
                "\"input\":" + "\"" +
                firstSide + ";" +
                secondSide + ";" +
                thirdSide + "\"" +
                "}";
    }
}

