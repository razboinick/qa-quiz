package dto;

public class CreateTriangleFullDto {

    private String separator;
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getSeparator() {
        return separator;
    }

    public double getFirstSide() {
        return firstSide;
    }

    public void setFirstSide(double firstSide) {
        this.firstSide = firstSide;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public void setSecondSide(double secondSide) {
        this.secondSide = secondSide;
    }

    public double getThirdSide() {
        return thirdSide;
    }

    public void setThirdSide(double thirdSide) {
        this.thirdSide = thirdSide;
    }

    @Override
    public String toString() {
        return "{" +
                "\"separator\":" + "\"" +
                separator + "\"," +
                "\"input\":" + "\"" +
                firstSide + separator +
                secondSide + separator +
                thirdSide + "\"" +
                "}";
    }

    public String toStringAsIntegerValues() {
        return "{" +
                "\"separator\":" + "\"" +
                separator + "\"," +
                "\"input\":" + "\"" +
                Double.valueOf(firstSide).intValue() + separator +
                Double.valueOf(secondSide).intValue() + separator +
                Double.valueOf(thirdSide).intValue() + "\"" +
                "}";
    }
}
