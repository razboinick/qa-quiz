package model;

public class Triangle {

    private String id;
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setFirstSide(double firstSide) {
        this.firstSide = firstSide;
    }

    public double getFirstSide() {
        return firstSide;
    }

    public void setSecondSide(double secondSide) {
        this.secondSide = secondSide;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public void setThirdSide(double thirdSide) {
        this.thirdSide = thirdSide;
    }

    public double getThirdSide() {
        return thirdSide;
    }
}
