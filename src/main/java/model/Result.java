package model;

public class Result {

    private Double result;

    public void setResult(Double result) {
        this.result = result;
    }

    public Double getResult() {
        return result;
    }
}
